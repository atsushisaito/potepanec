require 'rails_helper'

RSpec.describe Potepan::OrdersController, type: :controller do
  describe "POST #populate" do
    let!(:variant) { create(:variant) }

    it "assigns the requested order to @order" do
      post :populate, params: { variant_id: variant.id }
      order = Spree::Order.first
      expect(assigns(:order)).to eq order
    end

    it "redirects to the :edit template" do
      post :populate, params: { variant_id: variant.id }
      expect(response).to redirect_to potepan_cart_path
    end

    it "has 1 line_item" do
      post :populate, params: { variant_id: variant.id }
      expect(assigns(:order).line_items.size).to eq 1
    end

    context "when adding 1 variant to cart" do
      it "has 1 variant in cart" do
        post :populate, params: { variant_id: variant.id, quantity: 1 }
        expect(assigns(:line_item).quantity).to eq 1
      end
    end

    context "when adding 2 variants to cart" do
      it "has 2 variants in cart" do
        post :populate, params: { variant_id: variant.id, quantity: 2 }
        expect(assigns(:line_item).quantity).to eq 2
      end
    end
  end

  describe "GET #edit" do
    let!(:order) { create(:order, guest_token: "abcdefg") }

    before do
      cookies.signed[:guest_token] = "abcdefg"
    end

    it "returns http success" do
      get :edit
      expect(response).to have_http_status(:success)
    end

    it "assigns the requested order to @order" do
      get :edit
      expect(assigns(:order)).to eq order
    end

    it "renders the :edit template" do
      get :edit
      expect(response).to render_template :edit
    end
  end
end
