require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe 'GET #show' do
    let(:product) { create(:variant) }

    it "returns http success" do
      get :show, params: { id: product.id }
      expect(response).to have_http_status(:success)
    end

    it "assigns the requested product to @product" do
      get :show, params: { id: product.id }
      expect(assigns(:product)).to eq product
    end

    it "renders the :show template" do
      get :show, params: { id: product.id }
      expect(response).to render_template :show
    end

    describe "related_products" do
      let(:chair) { create(:taxon, name: 'chair') }
      let(:chair_variant) { create(:variant) }
      let!(:chair_products) { create_list(:product, 2) }
      let!(:table_product) { create(:product) }

      before do
        chair_products.map do |product|
           product.taxons << chair
           product.variants << chair_variant
        end
      end

      it "assigns the requested related_products to @related_products" do
        get :show, params: { id: chair_variant.id }
        expect(assigns(:related_products)).to match_array(chair_products - [chair_variant.product])
      end

      it "does not have related_products of another taxon" do
        get :show, params: { id: chair_variant.id }
        expect(assigns(:related_products)).not_to include(table_product)
      end

      context "when chair_products are 4" do
        let!(:chair_products) { create_list(:product, 4) }

        it "has 3 related_products" do
          get :show, params: { id: chair_variant.id }
          expect(assigns(:related_products).size).to be 3
        end
      end

      context "when chair_products are 5" do
        let!(:chair_products) { create_list(:product, 5) }

        it "has 4 related_products" do
          get :show, params: { id: chair_variant.id }
          expect(assigns(:related_products).size).to be 4
        end
      end

      context "when chair_products are 6" do
        let!(:chair_products) { create_list(:product, 6) }

        it "has 4 related_products" do
          get :show, params: { id: chair_variant.id }
          expect(assigns(:related_products).size).to be 4
        end
      end
    end
  end

  describe "GET #index" do
    render_views
    let!(:color) { create(:option_type, name: 'tshirt-color') }
    let!(:size) { create(:option_type, name: 'tshirt-size') }
    let!(:products) { create_list(:product, 2, :with_image) }

    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end

    it "assigns products to @products" do
      get :index
      expect(assigns(:products)).to match_array products
    end

    it "renders the :index template" do
      get :index
      expect(response).to render_template :index
    end

    describe "display_type" do
      context "when display type is grid" do
        it "returns product grid page" do
          get :index, params: { display_type: 'grid' }
          expect(response).to render_template(partial: '_product_grid')
        end
      end

      context "when display type is list" do
        it "returns product list page" do
          get :index, params: { display_type: 'list' }
          expect(response).to render_template(partial: '_product_list')
        end
      end
    end

    describe "filter by color" do
      let!(:color_list) { [black, white] }
      let(:black) { create(:option_value, :with_2variants, name: "Black", option_type_id: color.id) }
      let(:white) { create(:option_value, :with_2variants, name: "White", option_type_id: color.id) }

      it "assigns the requested color_list to @color_list" do
        get :index, params: { option_name: 'Black' }
        expect(assigns(:color_list)).to eq color_list
      end

      context "when params[:option_name] is Black" do
        it "has black products" do
          get :index, params: { option_name: 'Black' }
          expect(assigns(:products)).to match_array black.variants
        end

        it "does not have any white products" do
          get :index, params: { option_name: 'Black' }
          expect(assigns(:products)).not_to include(white.variants.first, white.variants.last)
        end
      end
    end

    describe "sort" do
      let!(:products) do
        [
          create(:product, :with_image, price: 100, available_on: 3.days.ago),
          create(:product, :with_image, price: 200, available_on: 4.days.ago)
        ]
      end

      describe "sort by available_on" do
        context "when params[:sort] is empty" do
          it "is sorted by newest" do
            get :index
            expect(assigns(:products)).to match products
          end
        end

        context "when params[:sort] is 新着順" do
          it "is sorted by newest" do
            get :index, params: { sort: "新着順" }
            expect(assigns(:products)).to match products
          end
        end

        context "when params[:sort] is 古い順" do
          it "is sorted by oldest" do
            get :index, params: { sort: "古い順" }
            expect(assigns(:products)).to match products.reverse
          end
        end
      end

      describe "sort by price" do
        context "when params[:sort] is 安い順" do
          it "is sorted by lowest" do
            get :index, params: { sort: "安い順" }
            expect(assigns(:products)).to match products
          end
        end

        context "when params[:sort] is 高い順" do
          it "is sorted by highest" do
            get :index, params: { sort: "高い順" }
            expect(assigns(:products)).to match products.reverse
          end
        end
      end
    end

    describe "filter by size" do
      let!(:size_list) { [s_size, m_size] }
      let(:s_size) { create(:option_value, :with_2variants, name: "S", option_type_id: size.id) }
      let(:m_size) { create(:option_value, :with_2variants, name: "M", option_type_id: size.id) }

      it "assigns the requested size_list to @size_list" do
        get :index, params: { option_name: 'S' }
        expect(assigns(:size_list)).to eq size_list
      end

      context "when params[:option_name] is S" do
        it "has s_size products" do
          get :index, params: { option_name: 'S' }
          expect(assigns(:products)).to match_array s_size.variants
        end

        it "does not have any m_size products" do
          get :index, params: { option_name: 'S' }
          expect(assigns(:products)).not_to include(m_size.variants.first, m_size.variants.last)
        end
      end
    end
  end

  describe "GET #search" do
    let!(:color) { create(:option_type, name: 'tshirt-color') }
    let!(:size) { create(:option_type, name: 'tshirt-size') }
    let!(:name_table) { create(:product, :with_image, name: 'Table') }
    let!(:description_table) { create(:product, :with_image, description: 'Table') }
    let!(:desk) { create(:product, :with_image, name: 'Desk', description: 'Desk') }

    it "renders the :index template" do
      get :search
      expect(response).to render_template :index
    end

    context "when search for table" do
      it "has products whose name or description including table" do
        get :search, params: { q: { name_or_description_cont: 'table' } }
        expect(assigns(:products)).to include(name_table, description_table)
      end

      it "does not have any products whose name or description not including table" do
        get :search, params: { q: { name_or_description_cont: 'table' } }
        expect(assigns(:products)).not_to include desk
      end
    end

    context "when search for abl" do
      it "has products whose name or description including abl" do
        get :search, params: { q: { name_or_description_cont: 'abl' } }
        expect(assigns(:products)).to include(name_table, description_table)
      end

      it "does not have any products whose name or description not including abl" do
        get :search, params: { q: { name_or_description_cont: 'abl' } }
        expect(assigns(:products)).not_to include desk
      end
    end

    context "when search for tabl%" do
      it "does not have any products whose name or description including table" do
        get :search, params: { q: { name_or_description_cont: 'tabl%' } }
        expect(assigns(:products)).not_to include(name_table, description_table)
      end
    end

    context "when search for chair" do
      it "has no products" do
        get :search, params: { q: { name_or_description_cont: 'chair' } }
        expect(assigns(:products)).to be_empty
      end
    end

    context "when search field is empty" do
      it "has all products" do
        get :search, params: { q: { name_or_description_cont: '' } }
        expect(assigns(:products)).to match_array [name_table, description_table, desk]
      end
    end
  end
end
