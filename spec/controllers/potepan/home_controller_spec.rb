require 'rails_helper'

RSpec.describe Potepan::HomeController, type: :controller do
  describe 'GET #index' do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end

    it "renders the :index template" do
      get :index
      expect(response).to render_template :index
    end

    describe 'new arrival products' do
      context "when products arrived last 2weeks are more than 20" do
        let!(:new_products) { create_list(:product, 21, available_on: 1.week.ago) }
        let!(:old_product) { create(:product, available_on: 1.month.ago) }

        it "has 20 products" do
          get :index
          expect(assigns(:new_arrival_products).size).to be 20
        end

        it "does not have any old_products" do
          get :index
          expect(assigns(:new_arrival_products)).not_to include(old_product)
        end
      end

      context "when products arrived last 2weeks are 20" do
        let!(:new_products) { create_list(:product, 20, available_on: 1.week.ago) }
        let!(:old_product) { create(:product, available_on: 1.month.ago) }

        it "has 20 new_products in random order" do
          get :index
          expect(assigns(:new_arrival_products)).to match_array new_products
        end

        it "does not have any old_products" do
          get :index
          expect(assigns(:new_arrival_products)).not_to include(old_product)
        end
      end

      context "when products arrived last 2weeks are less than 20" do
        let!(:new_products) { create_list(:product, 2, available_on: 1.week.ago) }
        let!(:old_product) { create_list(:product, 2, available_on: 1.month.ago) }
        let!(:new_arrival_products) { Spree::Product.order(available_on: :desc).first(20) }

        it "has 20 products sorted by newest arrivals" do
          get :index
          expect(assigns(:new_arrival_products)).to match new_arrival_products
        end
      end

      context "when products arrived last 2weeks are 0" do
        let!(:old_product) { create_list(:product, 2, available_on: 1.month.ago) }
        let!(:new_arrival_products) { Spree::Product.order(available_on: :desc).first(20) }

        it "has 20 old_products sorted by newest arrivals" do
          get :index
          expect(assigns(:new_arrival_products)).to match new_arrival_products
        end
      end

      context "when products arrived last 3months are 0" do
        let!(:old_product) { create(:product, available_on: 4.months.ago) }

        it "has no products" do
          get :index
          expect(assigns(:new_arrival_products)).to be_empty
        end
      end
    end

    describe 'popular categories' do
      let!(:popular_categories) { [create(:taxon, name: "T-Shirts"), create(:taxon, name: "Bags")] }

      it "assigns the requested popular_categories to @popular_categories" do
        get :index
        expect(assigns(:popular_categories)).to match_array popular_categories
      end
    end
  end
end
