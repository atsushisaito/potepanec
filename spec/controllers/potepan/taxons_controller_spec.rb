require 'rails_helper'

RSpec.describe Potepan::TaxonsController, type: :controller do
  describe 'GET #show' do
    render_views
    let!(:category) { create(:taxon) }
    let!(:root_categories) { Spree::Taxon.root_categories }
    let!(:color) { create(:option_type, name: 'tshirt-color') }
    let!(:size) { create(:option_type, name: 'tshirt-size') }
    let!(:products) { create_list(:product, 2, :with_image) }

    before do
      category.products << products
    end

    it "returns http success" do
      get :show, params: { id: category.id }
      expect(response).to have_http_status(:success)
    end

    it "assigns the requested taxon to @category" do
      get :show, params: { id: category.id }
      expect(assigns(:category)).to eq category
    end

    it "assigns the requested root_categories to @root_categories" do
      get :show, params: { id: category.id }
      expect(assigns(:root_categories)).to eq root_categories
    end

    it "assigns the requested products to @products" do
      get :show, params: { id: category.id }
      expect(assigns(:products)).to match_array products
    end

    it "renders the :show template" do
      get :show, params: { id: category.id }
      expect(response).to render_template :show
    end

    describe "display_type" do
      context "when display type is grid" do
        it "returns product grid page" do
          get :show, params: { id: category.id, display_type: 'grid' }
          expect(response).to render_template(partial: '_product_grid')
        end
      end

      context "when display type is list" do
        it "returns product list page" do
          get :show, params: { id: category.id, display_type: 'list' }
          expect(response).to render_template(partial: '_product_list')
        end
      end
    end

    describe "sort" do
      let!(:products) do
        [
          create(:product, :with_image, price: 100, available_on: 3.days.ago),
          create(:product, :with_image, price: 200, available_on: 4.days.ago)
        ]
      end

      describe "sort by available_on" do
        context "when params[:sort] is empty" do
          it "is sorted by newest" do
            get :show, params: { id: category.id }
            expect(assigns(:products)).to match products
          end
        end

        context "when params[:sort] is 新着順" do
          it "is sorted by newest" do
            get :show, params: { id: category.id, sort: "新着順" }
            expect(assigns(:products)).to match products
          end
        end

        context "when params[:sort] is 古い順" do
          it "is sorted by oldest" do
            get :show, params: { id: category.id, sort: "古い順" }
            expect(assigns(:products)).to match products.reverse
          end
        end
      end

      describe "sort by price" do
        context "when params[:sort] is 安い順" do
          it "is sorted by lowest" do
            get :show, params: { id: category.id, sort: "安い順" }
            expect(assigns(:products)).to match products
          end
        end

        context "when params[:sort] is 高い順" do
          it "is sorted by highest" do
            get :show, params: { id: category.id, sort: "高い順" }
            expect(assigns(:products)).to match products.reverse
          end
        end
      end
    end
  end
end
