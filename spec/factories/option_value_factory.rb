FactoryGirl.define do
  trait :with_2variants do
    after(:create) do |option_value|
      option_value.variants << create_list(:variant, 2) do |variant|
        variant.images << create(:image)
      end
    end
  end
end
