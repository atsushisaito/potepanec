FactoryGirl.define do
  trait :with_2products do
    after(:create) do |taxon|
      taxon.variants << create_list(:product, 2) do |product|
        product.images << create(:image)
      end
    end
  end
end
