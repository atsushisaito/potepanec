FactoryGirl.define do
  trait :with_image do
    after(:create) do |product|
      product.images << create(:image)
    end
  end
end
