require 'rails_helper'

RSpec.feature "Cart", type: :feature do
  feature "Adds and updates variant" do
    let(:table) { create(:variant, :with_image) }

    scenario "Adds and updates variant in cart" do
      visit potepan_product_path(table.id)
      expect(page).to have_content(table.name)

      select "1", from: "quantity"
      click_on "カートへ入れる"
      expect(current_url).to eq potepan_cart_url
      order = Spree::Order.last
      line_item = order.line_items.first
      expect(line_item.quantity).to eq 1

      visit potepan_product_path(table.id)
      select "1", from: "quantity"
      click_on "カートへ入れる"
      line_item.reload
      expect(line_item.quantity).to eq 2

      fill_in "order_line_items_attributes_0_quantity", with: "3"
      click_on "アップデート"
      expect(current_url).to eq potepan_cart_url
      line_item.reload
      expect(line_item.quantity).to eq 3

      fill_in "order_line_items_attributes_0_quantity", with: "0"
      click_on "アップデート"
      expect(current_url).to eq potepan_cart_url
      expect(order.line_items).to be_empty
    end

    scenario "Adds and updates variant in topbar" do
      visit potepan_product_path(table.id)
      expect(page).to have_content(table.name)

      select "1", from: "quantity"
      click_on "カートへ入れる"
      order = Spree::Order.last
      line_item_table = order.line_items.first

      visit potepan_root_path
      expect(page).to have_content(line_item_table.name)

      desk = create(:variant, :with_image)
      visit potepan_product_path(desk.id)
      select "1", from: "quantity"
      click_on "カートへ入れる"
      line_item_desk = order.line_items.last

      visit potepan_root_path
      expect(page).to have_content(line_item_table.name)
      expect(page).to have_content(line_item_desk.name)

      visit potepan_cart_path
      fill_in "order_line_items_attributes_0_quantity", with: "0"
      click_on "アップデート"

      visit potepan_root_path
      expect(page).not_to have_content(line_item_table.name)
      expect(page).to have_content(line_item_desk.name)
    end
  end
end
