module SidebarList
  def sidebar_list
    @root_categories = Spree::Taxon.root_categories
    @color_list = Spree::OptionType.option_values('tshirt-color')
    @size_list = Spree::OptionType.option_values('tshirt-size')
  end
end
