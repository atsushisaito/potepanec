module ParamOption
  def param_option
    @display_type = params[:display_type] || 'grid'
    @sort_option = %w(新着順 安い順 高い順 古い順)
  end
end
