class Potepan::ProductsController < ApplicationController
  include ParamOption
  include SidebarList
  include HasCartContext
  before_action :set_order, only: [:show, :index, :search]

  def show
    @q = Spree::Product.ransack(params[:q])
    @product = Spree::Variant.find(params[:id])
    @product_properties = @product.product.product_properties.includes(:property)
    @related_products = Spree::Product.related_products(@product.product).sample(4)
  end

  def index
    set_action
  end

  def search
    set_action
    render :index
  end

  private

  def products
    if params[:option_name].present?
      @products = @option_value.variants.sorted_by(params[:sort])
    elsif params[:q]
      @products = @q.result.sorted_by(params[:sort])
    else
      @products = Spree::Product.all.sorted_by(params[:sort])
    end
  end

  def set_action
    @option_value = Spree::OptionValue.find_by(name: params[:option_name])
    @q = Spree::Product.ransack(params[:q])
    sidebar_list
    param_option
    products
  end
end
