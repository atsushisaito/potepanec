class Potepan::TaxonsController < ApplicationController
  include ParamOption
  include SidebarList
  include HasCartContext
  before_action :set_order, only: :show

  def show
    @q = Spree::Product.ransack(params[:q])
    param_option
    sidebar_list
    @category = Spree::Taxon.find(params[:id])
    @category_products = Spree::Product.category_products(@category)
    @products = @category_products.sorted_by(params[:sort])
  end
end
