class Potepan::HomeController < ApplicationController
  include HasCartContext
  before_action :set_order, only: :index

  def index
    @q = Spree::Product.ransack(params[:q])
    @new_arrival_products = Spree::Product.new_arrival
    popular_category_list = ["T-Shirts", "Bags", "Mugs"]
    @popular_categories = Spree::Taxon.where(name: popular_category_list)
  end
end
