class Potepan::OrdersController < ApplicationController
  include HasCartContext
  before_action :assign_order, only: :update
  before_action :set_order, only: :edit

  def edit
    @q = Spree::Product.ransack(params[:q])
  end

  def update
    @order.contents.update_cart(order_params)
    redirect_to potepan_cart_path
  end

  def populate
    @order   = current_order(create_order_if_necessary: true)
    variant  = Spree::Variant.find(params[:variant_id] || params[:order][:variant_id])
    quantity = params[:quantity].present? ? params[:quantity].to_i : 1
    @line_item = @order.contents.add(variant, quantity)
    redirect_to potepan_cart_path
  end

  private

  def order_params
    if params[:order]
      params[:order].permit(line_items_attributes: [:id, :quantity])
    else
      {}
    end
  end

  def assign_order
    @order = current_order
    unless @order
      flash[:error] = t('spree.order_not_found')
      redirect_to(potepan_root_path) && return
    end
  end
end
