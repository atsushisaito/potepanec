Spree::Taxon.class_eval do
  class << self
    def root_categories
      taxonomies = Spree::Taxonomy.all
      where(taxonomy_id: taxonomies.ids, parent_id: nil)
    end
  end
end
