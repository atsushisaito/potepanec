Spree::Product.class_eval do
  scope :category_products, ->(taxon) { joins(:taxons).where(spree_taxons: { id: taxon.self_and_descendants.ids }) }
  scope :related_products, ->(product) { joins(:taxons).where(spree_taxons: { id: product.taxons.ids }).except_self(product).distinct }
  scope :except_self, ->(product) { where.not(id: product.id) }
  scope :within_2weeks, -> { where(available_on: 2.weeks.ago..Time.zone.now) }
  scope :within_3months, -> { where(available_on: 3.months.ago..Time.zone.now) }

  class << self
    def new_arrival
      num = 20
      within_2weeks.count >= num ? within_2weeks.sample(num) : within_3months.order(available_on: :desc).first(num)
    end

    def sorted_by(sort_type = "新着順")
      case sort_type
      when "新着順"
        order(available_on: :desc)
      when "安い順"
        ascend_by_master_price
      when "高い順"
        descend_by_master_price
      when "古い順"
        order(available_on: :asc)
      else
        order(available_on: :desc)
      end
    end

    def ransackable_attributes(_auth_object = nil)
      %w(name description)
    end
  end
end
