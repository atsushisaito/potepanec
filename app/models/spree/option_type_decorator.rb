Spree::OptionType.class_eval do
  scope :option_values, ->(option_type) { find_by(name: option_type).option_values }
end
