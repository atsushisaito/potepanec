Spree::Variant.class_eval do
  class << self
    def sorted_by(sort_type = "新着順")
      case sort_type
      when "新着順"
        joins(:product).order("spree_products.available_on desc")
      when "安い順"
        joins(product: { master: :default_price }).order("spree_prices.amount asc")
      when "高い順"
        joins(product: { master: :default_price }).order("spree_prices.amount desc")
      when "古い順"
        joins(:product).order("spree_products.available_on asc")
      else
        joins(:product).order("spree_products.available_on desc")
      end
    end
  end
end
